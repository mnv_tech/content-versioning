const { createResources, getResource } = require('../../dao/resources');
const { log } = require('../../util/logger');

const create = async (data) => {
  log.info(`Running Service: ${JSON.stringify(data)}`);
  const results = await createResources(data);
  log.info(`Got data from the database ${results[0]}`);
  return results;
};

const get = async (data) => {
  log.info(`Running Service: ${JSON.stringify(data)}`);
  const result = await getResource(data);
  log.info(`Got data from the database ${result}`);
  return result;
};

module.exports = { create, get };
