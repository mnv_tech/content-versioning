const { getExample } = require('../../dao/example/example');
const { log } = require('../../util/logger');

const ExampleService = async (data) => {
  log.info(`Running Service: ${data}`);
  const result = await getExample();
  log.info(`Got data from the database ${result}`);
  return result;
};

module.exports = { ExampleService };
