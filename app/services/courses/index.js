const { createCourse, duplicateCourse, getCourse } = require('../../dao/courses');
const { log } = require('../../util/logger');

const create = async (data) => {
  log.info(`Running Service: ${JSON.stringify(data)}`);
  const results = await createCourse(data);
  log.info(`Got data from the database ${results[0]}`);
  return results;
};

const duplicate = async (data) => {
  log.info(`Running Service: ${JSON.stringify(data)}`);
  const result = await duplicateCourse(data);
  log.info(`Got data from the database ${result}`);
  return result;
};

const get = async (data) => {
  log.info(`Running Service: ${JSON.stringify(data)}`);
  const result = await getCourse(data);
  log.info(`Got data from the database ${result}`);
  return result;
};

module.exports = { create, duplicate, get };
