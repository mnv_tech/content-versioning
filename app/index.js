const { log } = require('./util/logger');
const configHelper = require('./config');
const theServer = require('./server');
const consulHelper = require('./util/consul');

const monitor = consulHelper.monitor;
const listen = consulHelper.listen;

const config = configHelper.config;
const rebuildConfig = configHelper.refreshConfig;

log.info(config, process.env);

consulHelper.setLogger(log);

if (config.INSTANA_ENABLED) {
  require('./util/instana.js');
}

// Check if we are using Consul
if (config.CONSUL_HOST !== 'disabled') {
  // If we are, go ahead and set up our kv watches
  (async () => {
    // Monitor our own keys
    log.info(`Monitoring Conul Path: ${config.CONSUL_PATH}`);
    await monitor(`${config.CONSUL_PATH}`);
    // Monitor shared 'url_' keys (across all stacks)
    await monitor(config.CONSUL_PATH, obj => /.*\/url_.*/.test(obj.Key));
    // Add listener func(s)
    listen(rebuildConfig);
  })();
}

// --------  Start the server  --------
const port = config.PORT;
const server = theServer.listen(port, () => log.info(`-----  Server started on port ${port} ------`))
  .on('error', e => log.error(e));
module.exports = server;
