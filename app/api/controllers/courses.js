const { create, duplicate, get } = require('../../services/courses');
const { log } = require('../../util/logger');

const createCourse = async (ctx) => {
  log.debug('Calling example service to grab data from database');
  let results;
  try {
    if (ctx.request.body.sourceCourseId) {
      results = await duplicate(ctx.request.body)
    } else {
      results = await create(ctx.request.body)
    }
    if (!results.courseId) {
      throw new Error({ message: 'Course not created' })
    }
    ctx.status = 204;
  } catch (e) {
    log.error(e);
    ctx.status = 500;
    ctx.body = e;
  }
  log.info(results);
};

const getCourseResources = async (ctx) => {
  ctx.params.includeResources = true;
  return getCourse(ctx);
};

const getCourse = async (ctx) => {
  log.debug('Calling example service to grab data from database');
  let results;
  try {
    const course = await get(ctx.params);
    if (!course.courseId) {
      ctx.status = 404;
      ctx.body = { message: 'Course id not found' };
    } else {
      ctx.status = 200;
      ctx.body = course
    }
  } catch (e) {
    log.error(e);
    ctx.status = 500;
    ctx.body = e;
  }
  log.info(results);
};

const getCourseDescendents = async (ctx) => {
  ctx.params.includeDescendents = true;
  return getCourse(ctx);
};


module.exports = {
  createCourse,
  getCourse,
  getCourseResources,
  getCourseDescendents
};
