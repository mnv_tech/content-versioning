const health = (ctx) => {
  ctx.status = 200;
  ctx.body = true;
};

const status = (ctx) => {
  ctx.body = {
    request_datetime: new Date().toISOString(),
    service_name: 'YOUR SERVICE NAME HERE',
    version: '1.0.0', // Tool version. Usually stored in consul (ex: config().PIPELINE_BRANCH)
    install_datetime: new Date().toISOString(), // Last deploy date. Usually stored in consul (ex: config().PIPELINE_BRANCH)
    component_status: {
      component1: {
        status: 'UP/DOWN',
        reason: 'provide reason if down'
      },
      component2: {
        status: 'UP/DOWN',
        reason: 'provide reason if down'
      }
    }
  };
};

module.exports = {
  health,
  status
};
