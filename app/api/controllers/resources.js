const { create, get } = require('../../services/resources');
const { log } = require('../../util/logger');

const createResources = async (ctx) => {
  let results;
  try {
    await create(ctx.request.body);
    // perhaps need to check to make sure that resources were created?
    ctx.status = 204;
  } catch (e) {
    log.error(e);
    ctx.status = 500;
    ctx.body = e;
  }
  log.info(results);
};

// This is still WIP
const getResource = async (ctx) => {
  let results;
  try {
    const resource = await get(ctx.params);
    if (!resource.resourceId) {
      ctx.status = 404;
      ctx.body = { message: 'Resource id not found' };
    } else {
      ctx.status = 200;
      ctx.body = resource
    }
  } catch (e) {
    log.error(e);
    ctx.status = 500;
    ctx.body = e;
  }
  log.info(results);
};

module.exports = {
  getResource,
  createResources,
};
