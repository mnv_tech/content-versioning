const { ExampleService } = require('../../services/example/example');
const { log } = require('../../util/logger');

const exampleControllerFunc = async (ctx) => {
  log.debug('Calling example service to grab data from database');
  const result = await ExampleService({ data: 'test' });
  log.info(result);
  const newMessage = result && result.test_value ? `It worked! ${result.test_value}` : `Didn't get DB`;
  // This is how you access the logger via ctx (convienence, requires )
  ctx.log.info('this is and alternate way to access the logger', ctx.request.ip, ctx.path);
  const data = {
    message: newMessage,
    data: { woot: 'Doh!' },
    successObject: {
      message: 'Validation works',
      code: 200
    }
  };
  ctx.status = 200;
  ctx.body = data;
};

module.exports = {
  exampleControllerFunc
};
