const consulConfig = require('./util/consul').config;
const { log } = require('./util/logger');
const {
  listen,
  setLogger,
  monitor,
  updateListeners
} = require('./util/consul');

setLogger(log);
//* **************************************************************************************************************
// This is your dynamic config file.  When your application needs to understand params to establiash its
// behavior, it should never reference 'process.env'.  Doing so breaks the pattern used to maintain
// dynamic config behavior.  Instead of 'process.env', import (require) this file
//
// const config = require('./config.js').config;
//
// Use 'config' now like you would normally use 'process.env': config.LOG_LEVEL instead of process.env.LOG_LEVEL
//
// In this way, the app will always refer to params that can be updated at runtime (if using Consul)
//* **************************************************************************************************************

// create a unique, global symbol name
// -----------------------------------
const pjson = require('./package.json');
const CONFIG_KEY = Symbol.for(`config.${pjson.name}`);

// check if the global object has this symbol
// add it if it does not have the symbol, yet
// ------------------------------------------

const globalSymbols = Object.getOwnPropertySymbols(global);
const hasResolvedConfig = (globalSymbols.indexOf(CONFIG_KEY) > -1);

const resolveAppConfig = () => {
  Object.assign(global[CONFIG_KEY], {
    //* ***************************************************************************************************
    // A P P   P A R A M S   A R E   H E R E (NON DYNAMIC)
    //* ***************************************************************************************************
    NODE_ENV: process.env.NODE_ENV,
    HOSTNAME: process.env.HOSTNAME || 'template.local-mml.cloud',
    SCHEMES: process.env.SCHEMES || (process.env.NODE_ENV === 'development') ? ['http'] : ['http', 'https'],
    PORT: process.env.PORT || process.env.APP_PORT || 8000,
    CONSUL_HOST: process.env.CONSUL_HOST || 'consul.sh.mml.cloud',
    CONSUL_PORT: process.env.CONSUL_PORT || '80',
    // https://macmillanlearning.atlassian.net/wiki/spaces/SRE/pages/373784908/Consul+Key+Prefixes
    CONSUL_PATH: process.env.CONSUL_PATH,

    // ----------------------------------------------------------------------------------------------------
    // A D D   D Y N A M I C   P A R A M S   H E R E
    // ----------------------------------------------------------------------------------------------------
    // Options below this point will be provided by Consul
    // If CONSUL_HOST === 'disabled', then the option will
    // be provided by process.env
    // ----------------------------------------------------------------------------------------------------
    // TODO: Add all the Consul equivelant param sources (like log_level)
    EXAMPLE_DYNAMIC_OPTION: process.env.EXAMPLE_DYNAMIC_OPTION,
    // Bunyan log levels
    LOG_LEVEL: consulConfig().log_level || process.env.LOG_LEVEL || 'debug',
    // Redirect herte when an Achieve launch request fails on bad JWT
    LAUNCH_FAIL_REDIRECT: consulConfig().launch_fail_redirect || process.env.LAUNCH_FAIL_REDIRECT,
    // This is what your Achieve clients will sign JWT with
    USER_AUTH_SECRET: process.env.USER_AUTH_SECRET || 'local-secret',
    // The secret for verifying or signing calls to other servers
    SERVER_TO_SERVER_SECRET: consulConfig().SERVER_TO_SERVER_SECRET || process.env.SERVER_TO_SERVER_SECRET || 'local-s2s-secret',
    // Where to publish the Swagger UI docs (i.e. yourAppsHost:yourAppsPort/docs)
    SWAGGER_UI_URL: process.env.SWAGGER_UI_URL,

    PG_CONFIG: {
      host: process.env.PGHOST || process.env.PG_HOST || 'localhost',
      port: process.env.PGPORT || process.env.PG_PORT || '5433',
      user: process.env.PGUSER || process.env.PG_USER || 'postgres',
      password: process.env.PGPASSWORD || process.env.PG_PASSWORD || 'postgres',
      database: process.env.PGDATABASE || process.env.PG_DATABASE || 'db',
      debuglog: process.env.PG_DEBUGLOG || 0
    },
    ENCRYPTION: {
      encryptionKey: process.env.ENCRYPTION_KEY || '434B4683C348ACBAF23',
      initializationVector: process.env.ENCRYPTION_IV || ''
    }

    // ----------------------------------------------------------------------------------------------------
  });
};

if (!hasResolvedConfig) {
  global[CONFIG_KEY] = {};
  // Just do all of this once (regardless of how many times config.js is required...)
  const result = require('dotenv').config({path: '../.env'});
  log.info(result);
  resolveAppConfig();
}

// Setup keys to monitor
// Check if we are using Consul
if (global[CONFIG_KEY].CONSUL_HOST !== 'disabled') {
  const config = global[CONFIG_KEY];
  // If we are, go ahead and set up our kv watches
  (async () => {
    // Monitor our own keys
    log.info(`Monitoring Consul Path: ${config.CONSUL_PATH}`);
    await monitor(`${config.CONSUL_PATH}`);
    // Monitor shared 'url_' keys (across all stacks)
    await monitor(config.CONSUL_PATH, obj => /.*\/url_.*/.test(obj.Key));
    // TODO: monitor env/achieve/plat/SECRET_S2S instead of SERVER_TO_SERVER_SECRET for jwt validation
    // Add listener func(s)
    listen(resolveAppConfig);
    updateListeners();
  })();
}

// define the singleton API
// ------------------------

const singleton = {};

Object.defineProperty(singleton, 'instance', {
  get () {
    return global[CONFIG_KEY];
  }
});

// ensure the API is never changed
// -------------------------------

Object.freeze(singleton);

// export the singleton and the public func for rebuilding config when it is updated
// -----------------------------

module.exports = {
  config: singleton.instance,
  refreshConfig: resolveAppConfig
};
