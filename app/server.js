// Keep log and config init up top so that all init code hass access to it
const { log } = require('./util/logger');
const appConfig = require('./config').config;
const appUtils = require('./util/utils');

var logLevel = appUtils.getBunyanLogLevel(appConfig.LOG_LEVEL) || 'debug';
log.level(logLevel);
log.info(`Log level is set to ${appConfig.LOG_LEVEL} (${logLevel})`);
// Nothing but logging and app config above this line

//* *********************************************
// IMPORTS
//* *********************************************
const Koa = require('koa');
const Router = require('koa-router');
const Logger = require('koa-logger');
const Cors = require('@koa/cors');
const BodyParser = require('koa-bodyparser');
const Helmet = require('koa-helmet');
const respond = require('koa-respond');
const jwt = require('koa-jwt');
const openapi = require('./util/openapi');
const { ui } = require('swagger2-koa');
let instanaTracing = async (ctx, next) => { await next(); };
if (appConfig.INSTANA_ENABLED) {
  instanaTracing = require('./util/instana').tracingMiddleware;
}
//* *********************************************

const app = new Koa();
const router = new Router();

// ----------------------------------------------
// Server init
// ----------------------------------------------
// This is the koa request logger (not Bunyan)
app.use(Logger());
app.use(Helmet());
app.use(Cors());
app.use(BodyParser({
  enableTypes: ['json'],
  jsonLimit: '5mb',
  strict: true,
  onerror: function (ctx) {
    ctx.throw('body parse error', 422);
  }
}));

app.use(respond());

// This bootstraps JWT and then stores the jwt middleware inside jwtCheck
// Add jwtCheck to the routeOptions below
// To create an EP that is open, add it to the route options list
// and omit the jwtCheck middleware.
const jwtCheck = jwt({ secret: appConfig.USER_AUTH_SECRET, key: 'jwtBody', cookie: 'id_token' });

// This middleware is used for redirecting an EP that fails auth.  It is not used by default
// To use it for an EP, go to 'routeOptions' below, add the EP as the key and for the value,
// add 'redirecter' middleware BEFORE the jwtCheck middleware.
const redirecter = async (ctx, next) => {
  return next().catch((err) => {
    if (err.status === 401) {
      ctx.redirect(`${appConfig.LAUNCH_FAIL_REDIRECT}?${appConfig.LAUNCH_FAIL_REDIRECT_RETURN}=${encodeURIComponent(ctx.href)}`);
    } else {
      throw err;
    }
  });
};

const contextExtender = async (ctx, next) => {
  // Logger config is in ./util/logger.js
  // Attach the Bunyan logger to incoming requests
  ctx.log = log;
  ctx.appConfig = appConfig;

  return next();
};

// ----------------------------------------------
// Endpoint init (via Open API)
// ----------------------------------------------
// load the openapi document
const openApiPath = './api/openapi';
const openApiFile = 'openapi.yml';
const openApiDocument = openapi.load(openApiPath, openApiFile);

// add the routes defined in openapi.yml
const routeOptions3 = {
  controllerPath: '../api/controllers/',
  middleware: {
    default: []
  }
};
openapi.addRoutes(router, openApiDocument, routeOptions3);

// add middleware to serve ui for documentation generated from openapi routes
app.use(ui(openApiDocument, '/docs'));
// END Open API 3.0 ----------------------------------------------

// Get JWT bootstrapped
// initJwt adds middleware for dealing with 401.  It MUST come after swagger's middleware
// initJwt(app);
// app.use(jwt({ secret: 'server-to-server-secret', key: 'jwtBody', cookie: 'id_token'}));

// Set the routes in Koa
app.use(router.routes());
app.use(router.allowedMethods());

module.exports = app;
// END SERVER INIT

//* *********************************************
// HELPER FUNCS
//* *********************************************
