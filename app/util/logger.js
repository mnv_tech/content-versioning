const bunyan = require('bunyan');
var pjson = require('../package.json');
const LOGGER_KEY = Symbol.for(`logger.${pjson.name}`);

// check if the global object has this symbol
// add it if it does not have the symbol, yet
// ------------------------------------------

var globalSymbols = Object.getOwnPropertySymbols(global);
var haveLogger = (globalSymbols.indexOf(LOGGER_KEY) > -1);

if (!haveLogger) {
  // Create the logger and add it as a singleton
  global[LOGGER_KEY] = bunyan.createLogger({
    name: 'myapp',
    streams: [
      {
        level: 'debug',
        stream: process.stdout // log TRACE and above to stdout
      },
      {
        level: 'info',
        path: '/var/tmp/myapp-error.log', // log INFO and above to a file  TODO: Consul
        type: 'rotating-file',
        period: '1d', // daily rotation  TODO: Consul
        count: 3 // keep 3 back copies  TODO: Consul
      }
    ]
  });
}

// define the singleton API
// ------------------------

var singleton = {};

Object.defineProperty(singleton, 'instance', {
  get: function () {
    return global[LOGGER_KEY];
  }
});

// ensure the API is never changed
// -------------------------------

Object.freeze(singleton);

// export the singleton and the public func for rebuilding config when it is updated
// -----------------------------

module.exports = {
  log: singleton.instance
};
