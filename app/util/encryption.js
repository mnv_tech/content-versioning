import Crypto from 'crypto';
import { log } from './logger';
import { ENCRYPTION } from './config';

/**
 * A encryption key, in the context of symmetric cryptography, is something you keep secret. Anyone who knows your key (or can guess it)
 * can decrypt any data you've encrypted with it (or forge any authentication codes you've calculated with it, etc.).
 *
 * An IV or initialization vector is, in its broadest sense, just the initial value used to start some iterated process.
 * The term is used in a couple of different contexts, and implies different security requirements in each of them.
 * For example, cryptographic hash functions typically have a fixed IV, which is just an arbitrary constant
 * which is included in the hash function specification and is used as the initial hash value before any data is fed in.
 */
const configuredEncryptionKey = ENCRYPTION.encryptionKey;
const configuredEncryptionIv = ENCRYPTION.initializationVector;

/**
 * AES_128 - requires 16 byte key
 * AES_128_CBC - requires 16 byte key
 * AES_192 - requires 24 byte key
 * AES_256_CTR - requires 32 byte key
 */
const CIPHERS = {
  AES_128: 'aes128',
  AES_128_CBC: 'aes-128-cbc',
  AES_192: 'aes192',
  AES_256_CTR: 'aes-256-ctr'
};

const HASH = {
  SHA256: 'sha256'
};

/**
 * Encypt data provided, with the included cypher, encryption key, and encryption initializatin vector
 * @param {*} data - data to encrypt
 * @param {*} cipher - Encryption cypher to user (available ones listed in CIPHERS)
 * @param {*} encryptionKey - the encryption key
 * @param {*} iv - the intialization vector
 */
const encrypt = async (data, cipher = CIPHERS.AES_256_CTR, encryptionKey = configuredEncryptionKey, iv = configuredEncryptionIv) => {
  try {
    const setKey = Crypto.createHash('sha256').update(String(encryptionKey)).digest('base64').substr(0, 32);
    const setIv = iv || setKey.toString('hex').slice(0, 16);
    const encryptCipher = Crypto.createCipheriv(cipher, setKey, setIv);
    const encrypted = Buffer.concat([encryptCipher.update(Buffer.alloc(data, 'utf8')), encryptCipher.final()]);
    return encrypted.toString('base64');
  } catch (exception) {
    log.error(exception);
    throw new Error(exception.message);
  }
};

/**
 * Decrypt data provided, with the included cypher, encryption key, and encryption initializatin vector
 * @param {*} data - data to encrypt
 * @param {*} cipher - Encryption cypher to user (available ones listed in CIPHERS)
 * @param {*} encryptionKey - the encryption key
 * @param {*} iv - the intialization vector
 */
const decrypt = async (data, cipher = CIPHERS.AES_256_CTR, decryptionKey = configuredEncryptionKey, iv = configuredEncryptionIv) => {
  try {
    const setKey = Crypto.createHash('sha256').update(String(decryptionKey)).digest('base64').substr(0, 32);
    const setIv = iv || setKey.toString('hex').slice(0, 16);
    const decipher = Crypto.createCipheriv(cipher, setKey, setIv);
    const decrypted = Buffer.concat([decipher.update(data, 'base64'), decipher.final()]);
    return decrypted.toString('utf8');
  } catch (exception) {
    log.error(exception);
    throw new Error(exception.message);
  }
};

/**
 * Hashing of data via the specified hash algorithm
 * @param {*} data - data hashed
 * @param {*} hashAlgorithm - hashing algorithm to utilized
 */
const hash = async (data, hashAlgorithm = HASH.SHA256) => {
  try {
    const hashedValue = Crypto.createHash(hashAlgorithm).update(String(data)).digest('base64');
    return hashedValue;
  } catch (exception) {
    log.error(exception);
    throw new Error(exception.message);
  }
};

module.exports = { encrypt, decrypt, hash, CIPHERS, HASH };
