const { Pool } = require('pg');
const { log } = require('./logger');
const appConfig = require('../config');
const puresql = require('puresql');
const pool = new Pool(appConfig.config.PG_CONFIG);

// the pool with emit an error on behalf of any idle clients
// it contains if a backend error or network partition happens
pool.on('error', (err, client) => {
  log.error(`Unexpected error on idle client: ${err.message}`, err);
});

const debugFunction = query => log.info(query);

const getAdapter = async (client) => {
  if (!client) {
    client = await pool.connect()
  }
  return { adapter: puresql.adapters.pg(client, debugFunction), client }
};

const executeDao = async ({ operations, adapter, client }, { useTransaction, isIntermediate } = {}) => {
  try {
    if (!adapter) {
      const adapterBundle = await getAdapter(client);
      client = adapterBundle.client;
      adapter = adapterBundle.adapter;
    }
    if (useTransaction) {
      await client.query('BEGIN');
    }
    const results = await operations(adapter);
    if (useTransaction && !isIntermediate) {
      await client.query('COMMIT');
    }
    // return the client and adapter in addition to the results. This should allow chaining operations over the same connection
    return { results, client, adapter }
  } catch (e) {
    log.error(e);
    if (useTransaction) {
      await client.query('ROLLBACK');
    }
    throw e;
  } finally {
    if (client.release && !isIntermediate) { client.release(); }
  }
};

module.exports = { pool, getAdapter, executeDao };
