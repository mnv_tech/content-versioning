const fs = require('fs');
const yaml = require('js-yaml');

const SchemaValidator = require('openapi-schema-validator').default;
const RequestValidator = require('openapi-request-validator').default;
const ResponseValidator = require('openapi-response-validator').default;

const config = require('../config').config;
const { log } = require('./logger');

/**
 * read a yaml file and parse it
 * @param name
 * @returns {*}
 */
const readParse = (name) => {
  const content = fs.readFileSync(name, 'utf8');
  return yaml.safeLoad(content);
};

/**
 * if the objects in the map have a $ref key
 * replace the key with the contents of the file
 * the $ref key points to
 * @param folder - the folder to look for files in
 * @param map    - the map of objects resolve
 */
const resolveReferences = (folder, map) => {
  Object.keys(map).forEach((key) => {
    const referer = map[key];
    const reference = referer.$ref;
    if (reference) {
      const filename = `${folder}/${reference}`;
      log.debug(`referenced file ${filename}`);
      const content = readParse(filename);
      delete referer.$ref;
      Object.assign(referer, content);
    }
  });
};

/**
 * load an openapi definition file in yml form
 * and resolve references
 * and add some server entries based on env vars
 * and validate definition conforms to OpenAPI 3 spec
 * @param folder
 * @param file
 * @returns {*}
 */
const load = (folder, file) => {
  const root = readParse(`${folder}/${file}`);

  log.debug('Loading paths references in openapi document');
  resolveReferences(folder, root.paths);

  log.debug('Loading components.schema references in openapi document');
  resolveReferences(folder, root.components.schemas);

  config.SCHEMES.forEach((scheme) => {
    root.servers.push({ url: `${scheme}://${config.HOSTNAME}:${config.PORT}` });
  });

  const options = {
    version: 3
  };
  const validator = new SchemaValidator(options);

  try {
    const result = validator.validate(root);
    if (result.errors.length > 0) {
      result.errors.forEach((err) => {
        const fields = {
          error: err.message,
          path: err.dataPath,
          params: err.params
        };
        log.error(fields, 'OpenAPI schema error');
      });
      throw new Error('openapi.yml file fails to validate against OpenAPI 3 schema');
    }
  } catch (e) {
    log.error(e);
    log.error(root);
  }
  return root;
};

/**
 * load controller files referenced by x-swagger-router-controller
 * in the paths element of the openapi doc
 * @param controllerPath - folder containing controller files
 * @param paths - paths member of openapi document
 */
const loadControllers = (controllerPath, paths) => {
  const controllers = {};

  Object.keys(paths)
    .forEach((path) => {
      const controllerFile = paths[path]['x-swagger-router-controller'];
      if (!controllerFile) {
        throw new Error(`${path} is missing x-swagger-router-controller`);
      }
      if (controllers[controllerFile]) {
        return;
      }
      controllers[controllerFile] = require(`${controllerPath}${controllerFile}`);
      log.debug(`loaded controller ${controllerPath} ${controllerFile}`);
    });

  return controllers;
};

/**
 * convert path from OpenAPI format to KOA format
 * ex: /api/v1/courses/{courseId} to /api/v1/courses/:courseId
 * @param path
 */
const convertPath = (path) => {
  if (path.indexOf('{') < 0) {
    return path;
  }
  const parts = path.split('/');
  const convertedParts = parts.map((part) => {
    if (part.startsWith('{') && part.endsWith('}')) {
      let replace = part.replace('{', ':');
      replace = replace.substring(0, replace.length - 1);
      return replace;
    }
    return part;
  });
  return `${convertedParts.join('/')}`;
};

/**
 * convert strings to primitive values if they should be bool or int
 * not sophisticated, but perhaps sophisticated enuf
 * @param map
 */
const toPrimitive = (map) => {
  const lessStrings = {};
  Object.keys(map).forEach((name) => {
    const value = map[name];
    if (typeof value === 'string') {
      // maybe its really a boolean in string clothing
      const valueLowerCase = value.toLowerCase();
      if (['true', 'false'].includes(valueLowerCase)) {
        lessStrings[name] = valueLowerCase === 'true';
        return;
      }
      // maybe its really a number in string clothing
      if (/^\d+\.?\d*$|^\d*\.?\d+$/.test(value)) {
        lessStrings[name] = Number.parseInt(value, 10);
        return;
      }
    }
    lessStrings[name] = value;
  });
  return lessStrings;
};

/**
 * wrap the handler with request and response validation
 * @param pathMethodHandler
 * @param document
 */
const addValidation = (pathMethodHandler, document, path, method) => {
  const pathConfig = document.paths[path];
  const methodConfig = pathConfig[method];
  return async (ctx, next) => {
    log.debug(`OpenAPI request validation applied to ${ctx.path}`);

    const optionsRequest = {
      requestBody: methodConfig.requestBody,
      parameters: methodConfig.parameters,
      componentSchemas: document.components.schemas
    };
    const requestValidator = new RequestValidator(optionsRequest);

    const primitiveParams = toPrimitive(ctx.params);
    const primitiveQuery = toPrimitive(ctx.query);
    const primitiveHeaders = toPrimitive(ctx.request.headers);
    const request = {
      body: ctx.request.body,
      params: primitiveParams,
      query: primitiveQuery,
      headers: primitiveHeaders
    };
    const result = requestValidator.validate(request);
    if (result) {
      ctx.status = result.status;
      ctx.body = result.errors.map(e => ({
        path: e.path,
        message: e.message,
        location: e.location
      }));
      log.debug(`OpenAPI request validation FAILED ${ctx.path}`);
      log.debug(JSON.stringify(ctx.body, null, 2));
      return;
    }

    log.debug(`OpenAPI request validation PASSED ${ctx.path}`);

    await pathMethodHandler(ctx, next);

    log.debug(`OpenAPI response validation applied to ${ctx.path}`);

    const responseOptions = {
      responses: methodConfig.responses,
      components: document.components
    };
    const responseValidator = new ResponseValidator(responseOptions);

    const validationError = responseValidator.validateResponse(ctx.status, ctx.body);
    if (validationError) {
      ctx.status = 500;
      ctx.body = {
        message: validationError.message,
        errors: validationError.errors.map(e => ({
          path: e.path,
          message: e.message
        }))
      };
      log.debug(`OpenAPI response validation FAILED ${ctx.path}`);
      log.debug(JSON.stringify(ctx.body, null, 2));
      return;
    }

    log.debug(`OpenAPI response validation PASSED ${ctx.path}`);
  };
};

/**
 * add routes to koa router, based on paths in openapi document
 * @param router
 * @param document
 * @param options
 */
const addRoutes = (router, document, options) => {
  if (!router) {
    throw new Error('router not provided.');
  }
  if (!document || !document.paths) {
    throw new Error('document.paths not provided.');
  }
  if (!options || !options.controllerPath) {
    throw new Error('options.controllerPath not provided.');
  }

  const paths = document.paths;
  const controllers = loadControllers(options.controllerPath, paths);

  Object.keys(paths).forEach((path) => {
    const pathConfig = paths[path];
    const controllerFile = pathConfig['x-swagger-router-controller'];
    const controller = controllers[controllerFile];

    const pathConfigKeys = Object.keys(pathConfig);
    if (pathConfigKeys.length < 2) {
      throw new Error(`no methods found for path ${path}`);
    }

    pathConfigKeys.forEach((key) => {
      const notMethod = ['x-swagger-router-controller', 'parameters', 'summary', 'description'];
      if (notMethod.includes(key)) {
        return;
      }

      const method = key;
      const functionName = pathConfig[method].operationId;
      if (!functionName) {
        throw new Error(`no operationId for ${method} - ${path}`);
      }

      const pathMethodHandler = controller[functionName];
      if (!pathMethodHandler) {
        throw new Error(`For route ${method} ${path}, ${controllerFile} is missing ${functionName}`);
      }
      const validatingPathMethodHandler = addValidation(pathMethodHandler, document, path, method);

      log.info(`Adding openapi route ${method} - ${path} - ${controllerFile} - ${functionName}`);

      let middleware = [];
      if (controller.MIDDLEWARE) {
        middleware = controller.MIDDLEWARE;
      } else if (options.middleware) {
        // Match the paths by startWith to allow for pattern matching,
        // i.e. /api/v1/dashboards covers all endpoints starting with that URL
        // HOWEVER, if you match the full route, then only apply those middleware
        // (to allow explicit setting of route, instead of wildcards, for exceptions to the rules)
        if (options.middleware[path]) {
          middleware = options.middleware[path];
        } else {
          Object.keys(options.middleware).forEach((middlewarePath) => {
            if (path.startsWith(middlewarePath) && options.middleware[middlewarePath].length > 0) {
              middleware.push(...options.middleware[middlewarePath]);
            }
          });
          if (middleware.length === 0 && options.middleware.default) {
            middleware.push(...options.middleware.default);
          }
        }
      }

      // convert path parameters from OpenAPI to KOA. eg: {userId}, to :userId
      const koaPath = convertPath(path);

      switch (method) {
        case 'get':
          router.get(koaPath, ...middleware, validatingPathMethodHandler);
          break;
        case 'post':
          router.post(koaPath, ...middleware, validatingPathMethodHandler);
          break;
        case 'put':
          router.put(koaPath, ...middleware, validatingPathMethodHandler);
          break;
        case 'delete':
          router.delete(koaPath, ...middleware, validatingPathMethodHandler);
          break;
        case 'head':
          router.head(koaPath, ...middleware, validatingPathMethodHandler);
          break;
        case 'options':
          router.options(koaPath, ...middleware, validatingPathMethodHandler);
          break;
        case 'patch':
          router.patch(koaPath, ...middleware, validatingPathMethodHandler);
          break;
        default:
          throw new Error(`HTTP Method not recognized: ${method}`);
      }
    });
  });
  log.debug('Finished adding openapi routes');
};

module.exports = { load, addRoutes };
