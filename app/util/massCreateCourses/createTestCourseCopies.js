const fetch = require('node-fetch');
const fs = require('fs');
const path = require( 'path');
const Promise = require('bluebird');

// Execute this file and it will create a template course and then attempt to create numCopies number of courses. It will also save files which describe what it's trying to do.
// edit values below to customize
const numResourcesPerCourse = 1500;
const numCopies = 1000;
const initialSourceCourseId = 'josh-test-1';

const makeResources = () => {
  const resources = [];
  for (let i = 1; i < numResourcesPerCourse; i++) {
    resources.push({resourceId: i.toString()})
  }
  return resources
};

const initialCourseBody = {
  courseId: initialSourceCourseId,
  resources: makeResources()
};

const makeTemplate = (body = initialCourseBody) => {
  return fetch('http://localhost:8000/api/v1/courses', {
    method: 'post',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' }
  });
};

const makeResourceRelation = (resource, i) => {
  return {resourceId: `${resource.resourceId}_${i}`, sourceResourceId: resource.resourceId}
};

const makeCopies = (numCopies, courseId, resources) => {
  const newCourses = [];
  for (let i = 1; i <= numCopies; i++) {
    const newCourseId = `${courseId}_${i}`;
    const newResources = resources.map(resource => makeResourceRelation(resource, i));
    const course = {courseId: newCourseId, sourceCourseId: courseId, resources: newResources};
    newCourses.push(course);
  }
  return newCourses
};

const getCourse = async (courseId) => {
  const res = await fetch(`http://localhost:8000/api/v1/courses/${courseId}/resources`);
  const body = await res.json();
  return body
};

const makeInitialCopies = async () => {
  const body = await getCourse(initialSourceCourseId);
  const initialCourses = makeCopies(numCopies, body.courseId, body.resources);
  const initialPath = path.join(__dirname, 'initialCourses.txt');
  fs.writeFile(initialPath, JSON.stringify(initialCourses), err => {
    if (err) throw err;
    console.log('link:', initialPath);
  });
  const initialResults = await Promise.map(initialCourses, body => {
    return makeTemplate(body)
  }, { concurrency: 2 });
  const initialStatuses = initialResults.map(({ status }) => status);
  const initialStatusPath = path.join(__dirname, 'initialStatus.txt');
  fs.writeFile(initialStatusPath, JSON.stringify(initialStatuses), err => {
    if (err) throw err;
    console.log('link:', initialStatusPath);
  })
};

makeInitialCopies();
