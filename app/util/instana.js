const instana = require('instana-nodejs-sensor');
const opentracing = require('opentracing');

let globalTracer;

const noop = () => {};
const shim = {
  startSpan: () => ({
    addTags: noop,
    logEvent: noop,
    setTag: noop,
    log: noop,
    finish: noop
  }),
  inject: noop,
  extract: noop
};

exports.initInstana = (name) => {
  instana({
    serviceName: name,
    tracing: { enabled: true }
  });
  opentracing.initGlobalTracer(instana.opentracing.createTracer());
  globalTracer = opentracing.globalTracer();
};

const getTracer = exports.getTracer = () => globalTracer || shim;

const startSpan = exports.startSpan = name => getTracer().startSpan(name,
  { childOf: instana.opentracing.getCurrentlyActiveInstanaSpanContext() });

exports.getSpan = () => instana.opentracing.getCurrentlyActiveInstanaSpanContext();

exports.tracingMiddleware = async (ctx, next) => {
  const Tracer = getTracer();
  const wireCtx = Tracer.extract(Tracer.FORMAT_HTTP_HEADERS, ctx.headers);
  const span = startSpan('http', { childOf: wireCtx });
  ctx.span = span;
  span.setTag(opentracing.Tags.HTTP_METHOD, ctx.method);
  span.setTag(opentracing.Tags.HTTP_URL, ctx.href);
  span.setTag('http.request_size', ctx.get('content-length') || 0);
  if (ctx._matchedRoute) {
    span.setTag('http.matched_route', ctx._matchedRoute);
  }
  await next();
  span.setTag(opentracing.Tags.HTTP_STATUS_CODE, ctx.status);
  span.setTag('http.response_size', ctx.length || 0);
  span.finish();
};
