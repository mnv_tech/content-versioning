const bunyan = require('bunyan');

exports.getCourseRole = function (metadataNamespace = {}, courseId, metadataGeneric = {}) {
  const superRole = metadataNamespace.admin ||
        metadataNamespace.support ||
        metadataGeneric.admin ||
        metadataGeneric.support ||
        // media users
        metadataGeneric.editor ||
        metadataNamespace.editor ||
        metadataGeneric.producer ||
        metadataNamespace.producer;
  if ((!metadataNamespace.course_data && !metadataGeneric.course_data) || superRole) {
    return superRole ? 'admin' : undefined;
  }
  const getMetadataInfo = (metadata, role) => {
    const courseData = metadata.course_data || {};
    return courseData[role] || [];
  };
  const combinedCourseDataStudent = getMetadataInfo(metadataNamespace, 'student').concat(getMetadataInfo(metadataGeneric, 'student'));
  const combinedCourseDataInstructor = getMetadataInfo(metadataNamespace, 'instructor').concat(getMetadataInfo(metadataGeneric, 'instructor'));

  if (combinedCourseDataInstructor.indexOf(courseId) > -1) {
    return 'instructor';
  }

  if (combinedCourseDataStudent.indexOf(courseId) > -1) {
    return 'student';
  }
  return 'student';
};

exports.isNullOrUndefined = function (theVar) {
  return (typeof (theVar) === 'undefined' || theVar == null);
};

exports.isNullOrUndefinedOrEmpty = function (theVar) {
  return (typeof (theVar) === 'undefined' || theVar == null || theVar === '');
};

exports.getBunyanLogLevel = (strLogLevel) => {
  switch (strLogLevel) {
    case 'TRACE':
      return bunyan.TRACE;
    case 'DEBUG':
      return bunyan.DEBUG;
    case 'INFO':
      return bunyan.INFO;
    case 'WARN':
      return bunyan.WARN;
    case 'ERROR':
      return bunyan.ERROR;
    default:
      return bunyan.INFO;
  }
};

// executes an array of promises one at a time serially
exports.forEachPromise = (items, fn) => {
  return items.reduce(async (promise, item) => {
    await promise;
    return fn(item);
  }, Promise.resolve())
};
