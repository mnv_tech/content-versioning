const translate = (translation = [], databaseReturn) => {
  const cleanReturn = {};
  translation.forEach(({ dbName, jsName, returnFunction, test }) => {
    if (test(databaseReturn, dbName)) {
      cleanReturn[jsName] = returnFunction(databaseReturn, dbName, translation)
    }
  });
  return cleanReturn;
};

const exists = (databaseReturn, key) => !!databaseReturn[key];

const isExistingArray = (databaseReturn, key) => databaseReturn[key] && Array.isArray(databaseReturn[key]);

const getValue = (databaseReturn, key) => databaseReturn[key];

const translateValue = (databaseReturn, key, translation) => databaseReturn[key].map(item => translate(translation, item));

const resources = {
  dbName: 'resources',
  jsName: 'resources',
  test: isExistingArray,
  returnFunction: translateValue
};

const descendents = {
  dbName: 'descendents',
  jsName: 'descendents',
  test: isExistingArray,
  returnFunction: translateValue
};

const treeLeft =   {
  dbName: 'lft',
  jsName: 'treeLeft',
  test: exists,
  returnFunction: getValue
};

const resourceId = {
  dbName: 'resources_id',
  jsName: 'resourceId',
  test: exists,
  returnFunction: getValue
};

const courseId = {
  dbName: 'courses_id',
  jsName: 'courseId',
  test: exists,
  returnFunction: getValue
};

const sourceResourceId = {
  dbName: 'source_resources_id',
  jsName: 'sourceResourceId',
  test: exists,
  returnFunction: getValue
};

const resourceAncestorId = {
  dbName: 'resource_tree_ancestor_id',
  jsName: 'resourceAncestorId',
  test: exists,
  returnFunction: getValue
};

const courseGroupsId = {
  dbName: 'course_groups_id',
  jsName: 'courseGroupsId',
  test: exists,
  returnFunction: getValue
};



const coursesTranslation = [
  courseGroupsId,
  resourceAncestorId,
  sourceResourceId,
  resourceId,
  courseId,
  treeLeft,
  resources,
  descendents,
];

const resourcesTranslation = [
  resourceAncestorId,
  sourceResourceId,
  resourceId,
  courseId,
  treeLeft,
  resources,
  descendents,
];


// prime the translate function with the proper array of key value sets
// use translateCourse to clean up plural pg snake case column names into proper javascript camelCase names
const translateCourse = databaseReturn => translate(coursesTranslation, databaseReturn);
const translateResource = databaseReturn => translate(resourcesTranslation, databaseReturn);

module.exports = { translateCourse, translateResource };
