const { executeDao } = require('../../util/database');
const { log } = require('../../util/logger');
const { forEachPromise } = require('../../util/utils');
const { translateResource } = require('../translations');
const puresql = require('puresql');
const path = require('path');
const {
  CREATE_RESOURCES,
  CREATE_RESOURCE_TREE,
  GET_RESOURCE_PARAMETERS,
  UPDATE_RESOURCE_TREE
} = require('../queryNameConstants');

const resourceQueries = puresql.loadQueries(path.resolve(__dirname, 'resources.sql'));

const createDatabaseResources = async (resources, courseId, adapter) => {
  let resourceTreeInsertData = [];
  const resourcesInsertData = resources.map(resource => {
    resourceTreeInsertData.push([resource.resourceId, resource.resourceId, 1, 2]);
    return [resource.resourceId, resource.courseId || courseId]
  });
  await resourceQueries[CREATE_RESOURCES]({ resourcesInsertData }, adapter);
  return resourceQueries[CREATE_RESOURCE_TREE]({ resourceTreeInsertData }, adapter);
};

const duplicateDatabaseResources = async (resourceRelations, courseId, adapter) => {
  let resourceRelationsGroups = [[]];
  const resourceCounter = {};
  // handle the case where multiple resources with same sourceResourceId are duplicated at the same time
  resourceRelations.forEach(resource => {
    const { sourceResourceId } = resource;
    resourceCounter[sourceResourceId] = resourceCounter[sourceResourceId] ? resourceCounter[sourceResourceId] + 1 : 1;
    if (resourceRelationsGroups[resourceCounter[sourceResourceId] - 1]) {
      resourceRelationsGroups[resourceCounter[sourceResourceId] - 1].push(resource);
    } else {
      resourceRelationsGroups.push([resource])
    }
  });
  // This handles each group of resource relations. Each group can only contain a sourceResourceId only once or the queries won't make enough room in the tree
  const interpretResourceRelations = (courseId, adapter) => async (resourceRelations) => {
    const resourceMap = {};
    const sourceResourceIds = [];
    const resourcesInsertData = resourceRelations.map(resource => {
      resourceMap[resource.sourceResourceId] = resource.resourceId;
      sourceResourceIds.push(resource.sourceResourceId);
      return [resource.resourceId, resource.courseId || courseId]
    });
    const resourceCreateResults = await resourceQueries[CREATE_RESOURCES]({ resourcesInsertData }, adapter);
    const resourceParameters = await resourceQueries[GET_RESOURCE_PARAMETERS]({ sourceResourceIds }, adapter);
    const resourceTreeInsertData = resourceParameters.map(resource => {
      const { sourceResourceId, resourceAncestorId, treeLeft } = translateResource(resource);
      return [ resourceMap[sourceResourceId], resourceAncestorId, treeLeft + 1, treeLeft + 2 ]
    });
    const resourcesUpdatesResults = await resourceQueries[UPDATE_RESOURCE_TREE]({ sourceResourceIds }, adapter);
    return resourceQueries[CREATE_RESOURCE_TREE]({ resourceTreeInsertData }, adapter);
  };
  return forEachPromise(resourceRelationsGroups, interpretResourceRelations(courseId, adapter));
};

const createResources = async resources => {
  const resourcesToCreate = [];
  const resourcesToDuplicate = resources.map(resource => {
    if (resource.sourceResourceId) {
      return resource
    } else {
      resourcesToCreate.push(resource)
    }
  }).filter(resource => !!resource);
  const createResourcesOperation = (resourcesToCreate, resourcesToDuplicate) => async (adapter) => {
    // create new resources that are not copies
    const newResources = await createDatabaseResources(resourcesToCreate, null, adapter);
    // duplicate resources that are copies of existing and modify tree
    const duplicatedResources = await duplicateDatabaseResources(resourcesToDuplicate, null, adapter);
    return { newResources, duplicatedResources }
  };
  const operations = createResourcesOperation(resourcesToCreate, resourcesToDuplicate);
  return await executeDao({ operations }, { useTransaction: true });
};

// This is still WIP
const getResource = async resources => {
  return true;
};

module.exports = {
  duplicateDatabaseResources,
  createResources,
  createDatabaseResources,
  getResource
};
