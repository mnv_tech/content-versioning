-- name: create_resources
INSERT INTO josh_test.resources (resources_id, courses_id) VALUES :resourcesInsertData RETURNING *;

-- name: create_resource_tree
INSERT INTO josh_test.resource_tree (resources_id, resource_tree_ancestor_id, lft, rgt) VALUES :resourceTreeInsertData RETURNING *;

-- name: update_resource_tree
UPDATE josh_test.resource_tree node
SET rgt = node.rgt + 2, lft = CASE
    WHEN node.lft = parents.lft THEN node.lft
    ELSE node.lft + 2
END
FROM josh_test.resource_tree parents
WHERE parents.resource_tree_ancestor_id = node.resource_tree_ancestor_id AND node.lft >= parents.lft AND (parents.resources_id IN :sourceResourceIds OR node.resources_id IN :sourceResourceIds) RETURNING node.*;

-- name: get_resource_parameters
SELECT lft, resources_id as source_resources_id, resource_tree_ancestor_id FROM josh_test.resource_tree WHERE resources_id IN :sourceResourceIds;
