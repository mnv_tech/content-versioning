const { pool } = require('../../util/database');
const { log } = require('../../util/logger');

// async/await - check out a client
const getExample = async () => {
  let client;
  try {
    client = await pool.connect();
    const results = await client.query('SELECT TEST_VALUE FROM test WHERE id = $1', ['df45c1b8-eb60-4f1d-888a-3d35ac4723d6']);
    return results.rows[0];
  } catch (e) {
    log.error(e);
    throw new Error(e);
  } finally {
    if (client) { client.release(); }
  }
};

module.exports = {
  getExample
};
