module.exports = {
  CREATE_COURSE_GROUP: 'create_course_group',
  CREATE_COURSE: 'create_course',
  GET_COURSE_PARAMETERS: 'get_course_parameters',
  UPDATE_COURSE_TREE: 'update_course_tree',
  INSERT_DUPLICATED_COURSE: 'insert_duplicated_course',
  GET_COURSE: 'get_course',
  GET_RESOURCES_BY_COURSE: 'get_resources_by_course',
  GET_COURSE_COURSE_DESCENDENTS: 'get_course_course_descendents',
  CREATE_RESOURCES: 'create_resources',
  CREATE_RESOURCE_TREE: 'create_resource_tree',
  UPDATE_RESOURCE_TREE: 'update_resource_tree',
  GET_RESOURCE_PARAMETERS: 'get_resource_parameters'
};
