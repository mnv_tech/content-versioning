const { executeDao } = require('../../util/database');
const { log } = require('../../util/logger');
const { translateCourse } = require('../translations');
const puresql = require('puresql');
const path = require('path');
const { createDatabaseResources, duplicateDatabaseResources } = require('../resources');
const {
  CREATE_COURSE_GROUP,
  CREATE_COURSE,
  GET_COURSE_PARAMETERS,
  UPDATE_COURSE_TREE,
  INSERT_DUPLICATED_COURSE,
  GET_COURSE,
  GET_RESOURCES_BY_COURSE,
  GET_COURSE_COURSE_DESCENDENTS,
} = require('../queryNameConstants');

const courseQueries = puresql.loadQueries(path.resolve(__dirname, 'courses.sql'));

const createCourse = async ({ resources, courseId }) => {
  const createCourseOperation = (resources, courseId) => async (adapter) => {
    // create a new course group - This is somewhat of a hack until we understand how we will use course groups
    const courseGroupResults = await courseQueries[CREATE_COURSE_GROUP]({ isbn: 1, version_number: null }, adapter);
    const { courseGroupsId } = translateCourse(courseGroupResults[0]);
    // create new course
    const rawCourseResults = await courseQueries[CREATE_COURSE]({ courseId, courseGroupsId, lft: 1, rgt: 2 }, adapter);
    // if new resources are specified, create new resources
    if (resources && resources.length) {
      await createDatabaseResources(resources, courseId, adapter);
    }
    return rawCourseResults
  };
  const operations = createCourseOperation(resources, courseId);
  const { results } = await executeDao({ operations }, { useTransaction: true });
  return translateCourse(results[0])
};

const duplicateCourse = async ({ resources, sourceCourseId, courseId }) => {
  const duplicateCourseOperation = (resources, sourceCourseId, courseId) => async (adapter) => {
    // find existing source course groups id and left value for source course
    const courseParameters = await courseQueries[GET_COURSE_PARAMETERS]({ sourceCourseId }, adapter);
    const { treeLeft, courseGroupsId } = translateCourse(courseParameters[0]);
    // update the left and right values for the source course id and its descendents
    const updateCourseResults = await courseQueries[UPDATE_COURSE_TREE]({ treeLeft, courseGroupsId }, adapter);
    // insert new course into the gap created just above
    const insertResults = await courseQueries[INSERT_DUPLICATED_COURSE]({ courseId, courseGroupsId, treeLeft }, adapter);
    // if new resources are specified, create new resources as copies of existing
    if (resources && resources.length) {
      const resourceResults = await duplicateDatabaseResources(resources, courseId, adapter);
    }
    return insertResults
  };
  const operations = duplicateCourseOperation(resources, sourceCourseId, courseId);
  const { results } = await executeDao({ operations }, { useTransaction: true });
  return translateCourse(results[0]);
};

const getCourse = async ({ courseId, includeResources, includeDescendents }) => {
  const getCourseOperation = (courseId, includeResources, includeDescendents) => async (adapter) => {
    // get course id
    const courseData = await courseQueries[GET_COURSE]({ courseId }, adapter);
    if (courseData && !courseData.length) {
      return {}
    }
    const results = courseData[0];
    // if requested get resources
    if (includeResources) {
      results.resources = await courseQueries[GET_RESOURCES_BY_COURSE]({ courseId }, adapter);
    }
    // if requested get descendents
    if (includeDescendents) {
      results.descendents = await courseQueries[GET_COURSE_COURSE_DESCENDENTS]({ courseId }, adapter);
    }
    return results
  };
  const operations = getCourseOperation(courseId, includeResources, includeDescendents);
  const { results } = await executeDao({ operations }, { useTransaction: true });
  return translateCourse(results);
};

module.exports = {
  createCourse,
  duplicateCourse,
  getCourse
};
