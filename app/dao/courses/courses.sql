-- name: create_course_group
INSERT INTO josh_test.course_groups (course_groups_id, isbn, version_number) VALUES (DEFAULT, :isbn, :version_number) RETURNING *;

-- name: create_course
INSERT INTO josh_test.courses (courses_id, course_groups_id, lft, rgt) VALUES (:courseId, :courseGroupsId, :lft, :rgt) RETURNING *;

-- name: get_course_parameters
SELECT lft, course_groups_id FROM josh_test.courses WHERE courses_id = :sourceCourseId;

-- name: update_course_tree
UPDATE josh_test.courses SET rgt = rgt + 2, lft = CASE
 WHEN lft = :treeLeft THEN lft
 ELSE lft + 2
 END
WHERE lft >= :treeLeft AND course_groups_id = :courseGroupsId RETURNING *;

-- name: insert_duplicated_course
INSERT INTO josh_test.courses (courses_id, course_groups_id, lft, rgt) VALUES (:courseId, :courseGroupsId, :treeLeft + 1, :treeLeft + 2) RETURNING *;

-- name: get_course
SELECT courses_id FROM josh_test.courses WHERE courses_id = :courseId;

-- name: get_resources_by_course
SELECT resources_id FROM josh_test.resources WHERE courses_id = :courseId;

-- name: get_course_course_descendents
SELECT
    node.courses_id
FROM
    josh_test.courses node INNER JOIN
    josh_test.courses parent USING (course_groups_id)
WHERE
    node.lft BETWEEN parent.lft AND parent.rgt
        AND parent.courses_id = :courseId
        AND node.courses_id != :courseId
ORDER BY node.lft;
