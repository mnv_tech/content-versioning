// require the Koa server
const server = require('../index');
// require supertest
const { log } = require('../util/logger');
const exampleControllerFile = require('../api/controllers/exampleControllerFile');

const { ExampleService } = require('../services/example/example');
// close the server after each test
afterAll(() => {
  log.debug('After all tests...');
  server.close();
});
afterEach(() => {
  log.debug('After each test...');
});

jest.mock('../services/example/example', () => ({
  ExampleService: jest.fn()
}));

describe('routes: example end point', () => {
  test('should respond as expected', async () => {
    const ctx = {
      log,
      path: '/test',
      request: {
        ip: '127.0.0.1'
      },
      type: 'application/json'
    };
    ExampleService.mockReturnValueOnce({
      test_value: 'RETREIVED VALUE',
      response: {
        status: 200,
        type: 'application/json',
        body: { message: 'It worked! RETREIVED VALUE' }
      }
    });

    // Run the actual test
    await exampleControllerFile.exampleControllerFunc(ctx);

    // Test response status
    expect(ctx.status).toEqual(200);
    // Test response type
    expect(ctx.type).toEqual('application/json');
    // Test simple scalar val
    expect(ctx.body.message).toEqual('It worked! RETREIVED VALUE');
    // Test that properties exist in sub-objects without testing specific vals
    // expect(Object.keys(response.body.successObject)).toEqual(expect.arrayContaining(['message', 'code']));
  });
});
