// require the Koa server
const server = require('../index');
// require supertest
const request = require('supertest');
const { log } = require('../util/logger');

// close the server after each test
afterAll(() => {
  log.debug('After all tests...');
  server.close();
});
afterEach(() => {
  log.debug('After each test...');
  server.close();
});

describe('routes: health', () => {
  test('should return 200 and true if all is well', async () => {
    const response = await request(server)
      .get('/health').then((res, err) => {
        log.error(err);
        if (err) throw err;
        expect(res.status).toEqual(200);
        expect(res.body).toEqual(true);
        expect(res.type).toEqual('application/json');
      });
  });
});

describe('routes: status', () => {
  test('should return data compliant with SRE status frontend', async () => {
    await request(server)
      .get('/status').then((res, err) => {
        log.error(err);
        if (err) throw err;

        expect(res.status).toEqual(200);
        expect(typeof res.body).toEqual('object');
        expect(Object.keys(res.body).sort()).toEqual([
          'component_status',
          'install_datetime',
          'request_datetime',
          'service_name',
          'version'
        ]);
        expect(res.type).toEqual('application/json');
      });
  });

});
