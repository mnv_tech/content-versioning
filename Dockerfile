FROM node:10-alpine

WORKDIR /usr/src/app

RUN apk add -U bash python "libpq" "postgresql-dev" "g++" "make"

COPY ./app/package.json /usr/src/app
COPY ./app/yarn.lock /usr/src/app

RUN yarn install

RUN apk del -U python "libpq" "postgresql-dev" "g++" "make"

COPY ./app/index.js /usr/src/app
COPY ./app/server.js /usr/src/app
COPY ./app/config.js /usr/src/app

COPY ./app/api /usr/src/app/api
COPY ./app/services /usr/src/app/services
COPY ./app/dao /usr/src/app/dao
COPY ./app/util /usr/src/app/util

EXPOSE 8000

CMD yarn dev
