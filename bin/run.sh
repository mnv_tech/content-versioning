cd ..
export log_level=INFO # DEBUG, INFO, WARNING, ERROR, CRITICAL

startDB() {
  echo "Starting up database... If not on ($PG_PORT) then on port 5435"
  docker-compose --log-level=$log_level up -d postgres
  sleep 5
  echo "Running migrations..."
  docker-compose --log-level=$log_level up -d postgres-migration
}

startApp() {
  echo "Starting up application... APP_PORT ($APP_PORT or defaults to 8000) / INSPECT_PORT ($INSPECT_PORT or defaults to 10000), PostMan scripts at ./postman/DevTools.postman_collection "
  docker-compose --log-level=$log_level up -d template
}

if [ -z "$1" ]; then
    read -p "Correct usage:  ./run.sh (build, db, app, all) , do you want to run all? (y/n)" -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      set -- "all"
    else
      echo "Ok, selected no, please use \"./run.sh all\" to run everything"
      exit 1;
    fi
    
fi

case $1 in
  build)
    echo "Copying .npmrc from home directory"
    cp ~/.npmrc app/
    docker-compose --log-level=$log_level build
    ;;
  db)
    startDB
    ;;
  app)
    startApp
    ;;
  all)
    startDB
    startApp
    ;;
  stop)
    docker-compose --log-level=$log_level down
    ;;
    
  *)
    echo "Please idenitify the action you want to conduct: (build, db, app, all)"
    ;;
esac

