INSERT INTO josh_test.course_groups (course_groups_id, isbn, version_number) VALUES (1,1,1),(2,1,2),(3,2,1),(4,3,1);
INSERT INTO josh_test.courses (courses_id, course_groups_id, lft, rgt) VALUES ('one',1,1,6),('one-one',1,2,5),('one-one-one',1,3,4),('two',3,1,8),('two-one',3,2,3),('two-two',3,4,7),('two-two-one',3,5,6),('three',4,1,10),('three-one',4,2,9),('three-one-one',4,3,8),('three-one-one-one',4,4,5),('three-one-one-two',4,6,7);
INSERT INTO josh_test.resources (resources_id, courses_id) VALUES ('A1','one'),('B1','one'),('C1','one'),('D1','one'),('A2','one-one'),('B2','one-one'),('C2','one-one'),('D2','one-one'),('A3','one-one-one'),('B3','one-one-one'),('C3','one-one-one'),('D3','one-one-one'),('E1','one-one-one'),('F1','two'),('G1','two'),('H1','two'),('F2-1','two-one'),('G2-1','two-one'),('H2-1','two-one'),('F2-2','two-two'),('G2-2','two-two'),('H2-2','two-two'),('F3','two-two-one'),('G3','two-two-one'),('H3','two-two-one'),('I1','three'),('J1','three'),('I2','three-one'),('J2','three-one'),('I3','three-one-one'),('J3','three-one-one'),('I4-1','three-one-one-one'),('J4-1','three-one-one-one'),('I4-2','three-one-one-two'),('J4-2','three-one-one-two'),('K1','three-one-one'),('K2-1','three-one-one-one'),('K2-2','three-one-one-two');
INSERT INTO josh_test.resource_tree (resources_id, resource_tree_ancestor_id, lft, rgt) VALUES ('A1','A1',1,6),('B1','B1',1,6),('C1','C1',1,6),('D1','D1',1,6),('A2','A1',2,5),('B2','B1',2,5),('C2','C1',2,5),('D2','D1',2,5),('A3','A1',3,4),('B3','B1',3,4),('C3','C1',3,4),('D3','D1',3,4),('E1','E1',1,2),('F1','F1',1,8),('G1','G1',1,8),('H1','H1',1,8),('F2-1','F1',2,3),('G2-1','G1',2,3),('H2-1','H1',2,3),('F2-2','F1',4,7),('G2-2','G1',4,7),('H2-2','H1',4,7),('F3','F1',5,6),('G3','G1',5,6),('H3','H1',5,6),('I1','I1',1,10),('J1','J1',1,10),('I2','I1',2,9),('J2','J1',2,9),('I3','I1',3,8),('J3','J1',3,8),('I4-1','I1',4,5),('J4-1','J1',4,5),('I4-2','I1',6,7),('J4-2','J1',6,7),('K1','K1',1,6),('K2-1','K1',2,3),('K2-2','K1',4,5);

/**
# Find all descendents of course_id 'one'
SELECT
    node.*
FROM
    josh_test.courses node INNER JOIN
    josh_test.courses parent using (course_groups_id)
WHERE
    node.lft BETWEEN parent.lft AND parent.rgt
        AND parent.courses_id = 'one'
        #AND node.course_groups_id = 1
ORDER BY node.lft;


# Find all resources that are in the J1 resource tree
SELECT
    CONCAT(REPEAT(' ',
                COUNT(parent.resources_id) - 1),
            node.resources_id) AS resource_id,
    (COUNT(parent.resources_id) - 1) AS depth
FROM
    josh_test.resource_tree node
        INNER JOIN
    josh_test.resource_tree parent USING (resource_tree_ancestor_id)
WHERE
    node.lft BETWEEN parent.lft AND parent.rgt
        AND resource_tree_ancestor_id = 'J1'
GROUP BY node.resources_id
ORDER BY node.lft;

# Find all resource ancestors of J4-2
SELECT
    parent.*
FROM
    josh_test.resource_tree node
        INNER JOIN
    josh_test.resource_tree parent USING (resource_tree_ancestor_id)
WHERE
    node.lft BETWEEN parent.lft AND parent.rght
        #AND resource_tree_ancestor_id = 'J1'
       AND node.resources_id = 'J4-2'
ORDER BY parent.lft;

# Find all resource descendents of J2

SELECT
    node.*
FROM
    josh_test.resource_tree node
        INNER JOIN
    josh_test.resource_tree parent USING (resource_tree_ancestor_id)
WHERE
    node.lft BETWEEN parent.lft AND parent.rgt
        AND resource_tree_ancestor_id = 'J1'
       AND parent.resources_id = 'J2'
ORDER BY parent.lft;

# Find all resources and the descendents of a course or course_group
SELECT
    node.*,
    childcourses.courses_id
FROM
	josh_test.courses
    INNER JOIN
    josh_test.resources using (courses_id)
    INNER JOIN
    josh_test.resource_tree parent using (resources_id)
        INNER JOIN
    josh_test.resource_tree node USING (resource_tree_ancestor_id)
		INNER JOIN
        josh_test.resources resources2 ON node.resources_id = resources2.resources_id
        inner join
        josh_test.courses childcourses ON resources2.courses_id = childcourses.courses_id
WHERE
    node.lft BETWEEN parent.lft AND parent.rgt
    AND courses.courses_id = 'two-two'
    #AND courses.course_groups_id = 1
ORDER BY node.resource_tree_ancestor_id, parent.lft, node.lft;

# Find all resources and the descendents of a course or course_group
SELECT
    node.*,
    childcourses.courses_id
FROM
	josh_test.courses
    INNER JOIN
    josh_test.resources using (courses_id)
    INNER JOIN
    josh_test.resource_tree parent using (resources_id)
        INNER JOIN
    josh_test.resource_tree node USING (resource_tree_ancestor_id)
		INNER JOIN
        josh_test.resources resources2 ON node.resources_id = resources2.resources_id
        inner join
        josh_test.courses childcourses ON resources2.courses_id = childcourses.courses_id
WHERE
    node.lft BETWEEN parent.lft AND parent.rgt
    AND courses.courses_id = 'three-one'
    #AND courses.course_groups_id = 1
ORDER BY node.resource_tree_ancestor_id, parent.lft, node.lft;

# Find all resources and the descendents of a course or course_group
SELECT
    node.*,
    childcourses.courses_id
FROM
	josh_test.courses
    INNER JOIN
    josh_test.resources using (courses_id)
    INNER JOIN
    josh_test.resource_tree parent using (resources_id)
        INNER JOIN
    josh_test.resource_tree node USING (resource_tree_ancestor_id)
		INNER JOIN
        josh_test.resources resources2 ON node.resources_id = resources2.resources_id
        inner join
        josh_test.courses childcourses ON resources2.courses_id = childcourses.courses_id
WHERE
    node.lft BETWEEN parent.lft AND parent.rgt
    AND courses.courses_id = 'three-one-one'
    #AND courses.course_groups_id = 1
ORDER BY node.resource_tree_ancestor_id, parent.lft, node.lft;
**/