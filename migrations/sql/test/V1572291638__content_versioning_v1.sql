--
-- PostgreSQL database dump
--

-- Dumped from database version 12.0 (Debian 12.0-2.pgdg100+1)
-- Dumped by pg_dump version 12.0

-- Started on 2019-10-28 14:18:01 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

----
---- TOC entry 8 (class 2615 OID 16412)
---- Name: josh_test; Type: SCHEMA; Schema: -; Owner: postgres
----
--
--CREATE SCHEMA josh_test;
--
--
--ALTER SCHEMA josh_test OWNER TO postgres;


--
-- TOC entry 207 (class 1259 OID 16430)
-- Name: course_groups; Type: TABLE; Schema: josh_test; Owner: postgres
--

CREATE TABLE josh_test.course_groups (
    course_groups_id integer NOT NULL,
    isbn character varying(128),
    version_number integer
);


ALTER TABLE josh_test.course_groups OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16428)
-- Name: course_groups_course_groups_id_seq; Type: SEQUENCE; Schema: josh_test; Owner: postgres
--

CREATE SEQUENCE josh_test.course_groups_course_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE josh_test.course_groups_course_groups_id_seq OWNER TO postgres;

--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 206
-- Name: course_groups_course_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: josh_test; Owner: postgres
--

ALTER SEQUENCE josh_test.course_groups_course_groups_id_seq OWNED BY josh_test.course_groups.course_groups_id;


--
-- TOC entry 203 (class 1259 OID 16413)
-- Name: courses; Type: TABLE; Schema: josh_test; Owner: postgres
--

CREATE TABLE josh_test.courses (
    courses_id character varying(128) NOT NULL,
    course_groups_id integer,
    lft integer NOT NULL,
    rgt integer NOT NULL
);


ALTER TABLE josh_test.courses OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16418)
-- Name: resource_tree; Type: TABLE; Schema: josh_test; Owner: postgres
--

CREATE TABLE josh_test.resource_tree (
    resources_id character varying(128) NOT NULL,
    resource_tree_ancestor_id character varying(128) NOT NULL,
    lft integer NOT NULL,
    rgt integer NOT NULL
);


ALTER TABLE josh_test.resource_tree OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16423)
-- Name: resources; Type: TABLE; Schema: josh_test; Owner: postgres
--

CREATE TABLE josh_test.resources (
    resources_id character varying(128) NOT NULL,
    courses_id character varying(128) NOT NULL
);


ALTER TABLE josh_test.resources OWNER TO postgres;

--
-- TOC entry 2792 (class 2604 OID 16433)
-- Name: course_groups course_groups_id; Type: DEFAULT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.course_groups ALTER COLUMN course_groups_id SET DEFAULT nextval('josh_test.course_groups_course_groups_id_seq'::regclass);


--
-- TOC entry 2816 (class 2606 OID 16435)
-- Name: course_groups course_groups_pkey; Type: CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.course_groups
    ADD CONSTRAINT course_groups_pkey PRIMARY KEY (course_groups_id);


--
-- TOC entry 2795 (class 2606 OID 16490)
-- Name: courses courses_pkey; Type: CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (courses_id);


--
-- TOC entry 2798 (class 2606 OID 16471)
-- Name: courses left_unique_courses; Type: CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.courses
    ADD CONSTRAINT left_unique_courses UNIQUE (course_groups_id, lft) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2804 (class 2606 OID 16505)
-- Name: resource_tree left_unique_resources; Type: CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.resource_tree
    ADD CONSTRAINT left_unique_resources UNIQUE (resource_tree_ancestor_id, lft) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2806 (class 2606 OID 16497)
-- Name: resource_tree resource_tree_pkey; Type: CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.resource_tree
    ADD CONSTRAINT resource_tree_pkey PRIMARY KEY (resources_id);


--
-- TOC entry 2814 (class 2606 OID 16519)
-- Name: resources resources_pkey; Type: CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.resources
    ADD CONSTRAINT resources_pkey PRIMARY KEY (resources_id);


--
-- TOC entry 2801 (class 2606 OID 16474)
-- Name: courses right_unique_courses; Type: CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.courses
    ADD CONSTRAINT right_unique_courses UNIQUE (course_groups_id, rgt) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2811 (class 2606 OID 16508)
-- Name: resource_tree right_unique_resources; Type: CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.resource_tree
    ADD CONSTRAINT right_unique_resources UNIQUE (resource_tree_ancestor_id, rgt) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2793 (class 1259 OID 16445)
-- Name: courses_course_groups_id_idx; Type: INDEX; Schema: josh_test; Owner: postgres
--

CREATE INDEX courses_course_groups_id_idx ON josh_test.courses USING btree (course_groups_id);


--
-- TOC entry 2796 (class 1259 OID 16476)
-- Name: left_courses; Type: INDEX; Schema: josh_test; Owner: postgres
--

CREATE INDEX left_courses ON josh_test.courses USING btree (course_groups_id, lft);


--
-- TOC entry 2802 (class 1259 OID 16511)
-- Name: left_resources; Type: INDEX; Schema: josh_test; Owner: postgres
--

CREATE INDEX left_resources ON josh_test.resource_tree USING btree (resource_tree_ancestor_id, lft);


--
-- TOC entry 2807 (class 1259 OID 16510)
-- Name: resource_tree_resource_tree_ancestor_id_idx; Type: INDEX; Schema: josh_test; Owner: postgres
--

CREATE INDEX resource_tree_resource_tree_ancestor_id_idx ON josh_test.resource_tree USING btree (resource_tree_ancestor_id);


--
-- TOC entry 2808 (class 1259 OID 16498)
-- Name: resource_tree_resources_id_idx; Type: INDEX; Schema: josh_test; Owner: postgres
--

CREATE INDEX resource_tree_resources_id_idx ON josh_test.resource_tree USING btree (resources_id);


--
-- TOC entry 2812 (class 1259 OID 16530)
-- Name: resources_courses_id_idx; Type: INDEX; Schema: josh_test; Owner: postgres
--

CREATE INDEX resources_courses_id_idx ON josh_test.resources USING btree (courses_id);


--
-- TOC entry 2799 (class 1259 OID 16477)
-- Name: right_courses; Type: INDEX; Schema: josh_test; Owner: postgres
--

CREATE INDEX right_courses ON josh_test.courses USING btree (course_groups_id, rgt);


--
-- TOC entry 2809 (class 1259 OID 16512)
-- Name: right_resources; Type: INDEX; Schema: josh_test; Owner: postgres
--

CREATE INDEX right_resources ON josh_test.resource_tree USING btree (resource_tree_ancestor_id, rgt);


--
-- TOC entry 2817 (class 2606 OID 16440)
-- Name: courses courses_course_groups_id_fkey; Type: FK CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.courses
    ADD CONSTRAINT courses_course_groups_id_fkey FOREIGN KEY (course_groups_id) REFERENCES josh_test.course_groups(course_groups_id);


--
-- TOC entry 2819 (class 2606 OID 16525)
-- Name: resource_tree resource_tree_resource_tree_ancestor_id_fkey; Type: FK CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.resource_tree
    ADD CONSTRAINT resource_tree_resource_tree_ancestor_id_fkey FOREIGN KEY (resource_tree_ancestor_id) REFERENCES josh_test.resources(resources_id);


--
-- TOC entry 2818 (class 2606 OID 16520)
-- Name: resource_tree resource_tree_resources_id_fkey; Type: FK CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.resource_tree
    ADD CONSTRAINT resource_tree_resources_id_fkey FOREIGN KEY (resources_id) REFERENCES josh_test.resources(resources_id);


--
-- TOC entry 2820 (class 2606 OID 16531)
-- Name: resources resources_courses_id_fkey; Type: FK CONSTRAINT; Schema: josh_test; Owner: postgres
--

ALTER TABLE ONLY josh_test.resources
    ADD CONSTRAINT resources_courses_id_fkey FOREIGN KEY (courses_id) REFERENCES josh_test.courses(courses_id);


-- Completed on 2019-10-28 14:18:02 CDT

--
-- PostgreSQL database dump complete
--

