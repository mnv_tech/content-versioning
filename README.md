###### To start using frontend
Start the server quickly via docker compose.  
```
Quick start:  
bin/run.sh build
bin/run.sh all

Or use docker commands
docker-compose --log-level=DEBUG build
docker-compose --log-level=DEBUG up -d

Run HTTP request (via Postman):
http://localhost:8000/status

Then a test endpoint,
http://localhost:8000/api/v1/test1/234-23-2342

or check out the REST API documentation provided by 
http://localhost:8000/docs

To take it down  
docker-compose --log-level=DEBUG down
```

To populate with some initial data, run `initialData.sql`

Data matches this structure: https://docs.google.com/drawings/d/1Lc2hRdnRopxMqdu0vIwOKUgvRQ6YcSUdwbK9Xomh3uM/edit

###### If you want to create a lot of courses
 `/app/util/massCreateCourses/`

* You need to edit the `createTestCourseCopies.js` file with a new main template courseId and the number of entities and copies you want.
* yarn inside `massCreateCourses`
* start service + DB if not already running
* `node createTestCourseCopies.js`


###### Service Highlights
1. Swagger Defined routes, with request/response validation
2. External configuration via Consul
3. Database setup for Postgres
4. 256 bit AES CTR Encryption / SHA256 Hashing
5. Instana Open Tracing
6. Dockerized

###### Docker Commands
build image  
`docker build --tag eventing-producer:local -f Dockerfile .`  

run image, pass in the PORT environment variable, set the docker port match  
`docker run -d -e PORT=8000 -p 8000:8000 eventing-producer:local`  

create container  
`docker create --name eventing-producer eventing-producer:local`  

start container  
`docker start eventing-producer`  

get a shell in a running container  
`docker exec -it eventing-producer sh`  

**Docker Compose**
build  
`docker-compose --log-level=DEBUG build`  

start, or build and start
`docker-compose --log-level=DEBUG up -d`  

```
If you get the following: ERROR: Pool overlaps with other one on this address space
Run: 
docker network ls
docker network rm local_achieve
```

off  
`docker-compose down`  
